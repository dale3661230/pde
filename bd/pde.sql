-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 27-08-2018 a las 06:13:45
-- Versión del servidor: 10.1.32-MariaDB
-- Versión de PHP: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `pde`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asignaciones_turnos`
--

CREATE TABLE `asignaciones_turnos` (
  `id` int(11) NOT NULL,
  `id_planilla` int(11) NOT NULL,
  `id_turno` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `disponibilidad` int(11) NOT NULL,
  `activo` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `asignaciones_turnos`
--

INSERT INTO `asignaciones_turnos` (`id`, `id_planilla`, `id_turno`, `id_usuario`, `disponibilidad`, `activo`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 0, 1, '2018-08-25 21:54:16', '2018-08-26 00:41:54'),
(2, 1, 3, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(3, 1, 5, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(4, 1, 6, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(5, 1, 11, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(6, 1, 12, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(7, 1, 13, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(8, 1, 14, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(9, 1, 15, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(10, 1, 16, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(11, 1, 20, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(12, 1, 21, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(13, 1, 23, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(14, 1, 24, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(15, 1, 25, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(16, 1, 26, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(17, 1, 27, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(18, 1, 28, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(19, 1, 29, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(20, 1, 30, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(21, 1, 31, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(22, 1, 32, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(23, 1, 33, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(24, 1, 34, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(25, 1, 35, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(26, 1, 36, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(27, 1, 37, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(28, 1, 38, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(29, 1, 39, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(30, 1, 40, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(31, 1, 41, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(32, 1, 42, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(33, 1, 43, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(34, 1, 44, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(35, 1, 45, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(36, 1, 46, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(37, 1, 47, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(38, 1, 48, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(39, 1, 49, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(40, 1, 50, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(41, 1, 51, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(42, 1, 52, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(43, 1, 53, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(44, 1, 54, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(45, 1, 55, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(46, 1, 56, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(47, 1, 57, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(48, 1, 58, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(49, 1, 59, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(50, 1, 60, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(51, 1, 61, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(52, 1, 62, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(53, 1, 63, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(54, 1, 64, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(55, 1, 65, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(56, 1, 66, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(57, 1, 67, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(58, 1, 68, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(59, 1, 69, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(60, 1, 70, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(61, 1, 71, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(62, 1, 72, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(63, 1, 73, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(64, 1, 74, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(65, 1, 75, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(66, 1, 76, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(67, 1, 77, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(68, 1, 78, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(69, 1, 79, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(70, 1, 80, 1, 0, 1, '2018-08-26 00:41:54', '2018-08-26 00:41:54'),
(71, 1, 81, 2, 0, 1, '2018-08-26 21:17:54', '2018-08-26 00:41:55'),
(72, 1, 82, 1, 0, 1, '2018-08-26 00:41:55', '2018-08-26 00:41:55'),
(73, 1, 83, 1, 0, 1, '2018-08-26 00:41:55', '2018-08-26 00:41:55'),
(74, 1, 84, 1, 0, 1, '2018-08-26 00:41:55', '2018-08-26 00:41:55'),
(75, 1, 85, 1, 0, 1, '2018-08-26 00:41:55', '2018-08-26 00:41:55'),
(76, 1, 86, 1, 0, 1, '2018-08-26 00:41:55', '2018-08-26 00:41:55'),
(77, 1, 87, 1, 0, 1, '2018-08-26 00:41:55', '2018-08-26 00:41:55'),
(78, 1, 88, 1, 0, 1, '2018-08-26 21:31:05', '0000-00-00 00:00:00'),
(79, 1, 89, 1, 0, 1, '2018-08-26 21:31:05', '0000-00-00 00:00:00'),
(80, 1, 90, 1, 0, 1, '2018-08-26 21:31:05', '0000-00-00 00:00:00'),
(82, 1, 92, 1, 0, 1, '2018-08-26 21:31:05', '0000-00-00 00:00:00'),
(83, 1, 93, 1, 0, 1, '2018-08-26 21:31:05', '0000-00-00 00:00:00'),
(84, 1, 94, 1, 0, 1, '2018-08-26 21:31:05', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dias`
--

CREATE TABLE `dias` (
  `id` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `id_plantilla` int(11) NOT NULL,
  `feriado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_turnos_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perfiles`
--

CREATE TABLE `perfiles` (
  `id` int(11) NOT NULL,
  `nombre` text NOT NULL,
  `active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `perfiles`
--

INSERT INTO `perfiles` (`id`, `nombre`, `active`) VALUES
(1, 'Administrador', 1),
(2, 'Empaque', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perfiles_usuarios`
--

CREATE TABLE `perfiles_usuarios` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_perfil` int(11) NOT NULL,
  `active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `perfiles_usuarios`
--

INSERT INTO `perfiles_usuarios` (`id`, `id_usuario`, `id_perfil`, `active`) VALUES
(1, 2, 1, 1),
(2, 2, 2, 1),
(3, 3, 2, 1),
(4, 4, 2, 1),
(5, 5, 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `planillas`
--

CREATE TABLE `planillas` (
  `id` int(11) NOT NULL,
  `fecha_comienzo` date NOT NULL,
  `fecha_termino` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `planillas`
--

INSERT INTO `planillas` (`id`, `fecha_comienzo`, `fecha_termino`) VALUES
(1, '2018-08-27', '2018-09-03');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `queqe`
--

CREATE TABLE `queqe` (
  `id` int(11) NOT NULL,
  `id_planilla` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `fecha_turno` date NOT NULL,
  `hora_inicio` time NOT NULL,
  `hora_termino` time NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `activo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `turnos`
--

CREATE TABLE `turnos` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_planilla` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `hora_inicio` time NOT NULL,
  `hora_termino` time NOT NULL,
  `activo` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `turnos`
--

INSERT INTO `turnos` (`id`, `id_planilla`, `fecha`, `hora_inicio`, `hora_termino`, `activo`, `created_at`, `updated_at`) VALUES
(1, 1, '2018-08-27', '08:00:00', '12:00:00', 0, NULL, NULL),
(3, 1, '2018-08-27', '08:00:00', '12:00:00', 0, NULL, NULL),
(5, 1, '2018-08-27', '12:00:00', '15:30:00', 0, NULL, NULL),
(6, 1, '2018-08-27', '12:00:00', '15:30:00', 0, NULL, NULL),
(11, 1, '2018-08-27', '15:30:00', '19:00:00', 0, NULL, NULL),
(12, 1, '2018-08-27', '15:30:00', '19:00:00', 0, NULL, NULL),
(13, 1, '2018-08-27', '17:00:00', '20:30:00', 0, NULL, NULL),
(14, 1, '2018-08-27', '17:00:00', '20:30:00', 0, NULL, NULL),
(15, 1, '2018-08-27', '19:00:00', '22:00:00', 0, NULL, NULL),
(16, 1, '2018-08-27', '19:00:00', '22:00:00', 0, NULL, NULL),
(20, 1, '2018-08-28', '08:00:00', '12:00:00', 0, NULL, NULL),
(21, 1, '2018-08-28', '08:00:00', '12:00:00', 0, NULL, NULL),
(23, 1, '2018-08-28', '12:00:00', '15:30:00', 0, NULL, NULL),
(24, 1, '2018-08-28', '12:00:00', '15:30:00', 0, NULL, NULL),
(25, 1, '2018-08-28', '15:30:00', '19:00:00', 0, NULL, NULL),
(26, 1, '2018-08-28', '15:30:00', '19:00:00', 0, NULL, NULL),
(27, 1, '2018-08-28', '17:00:00', '20:30:00', 0, NULL, NULL),
(28, 1, '2018-08-28', '17:00:00', '20:30:00', 0, NULL, NULL),
(29, 1, '2018-08-28', '19:00:00', '22:00:00', 0, NULL, NULL),
(30, 1, '2018-08-28', '19:00:00', '22:00:00', 0, NULL, NULL),
(31, 1, '2018-08-27', '10:00:00', '13:30:00', 0, NULL, NULL),
(32, 1, '2018-08-29', '08:00:00', '12:00:00', 0, NULL, NULL),
(33, 1, '2018-08-29', '10:00:00', '13:30:00', 0, NULL, NULL),
(34, 1, '2018-08-29', '19:00:00', '22:00:00', 0, NULL, NULL),
(35, 1, '2018-08-29', '19:00:00', '22:00:00', 0, NULL, NULL),
(36, 1, '2018-08-29', '17:00:00', '20:30:00', 0, NULL, NULL),
(37, 1, '2018-08-29', '17:00:00', '20:30:00', 0, NULL, NULL),
(38, 1, '2018-08-29', '15:30:00', '19:00:00', 0, NULL, NULL),
(39, 1, '2018-08-29', '15:30:00', '19:00:00', 0, NULL, NULL),
(40, 1, '2018-08-29', '12:00:00', '15:30:00', 0, NULL, NULL),
(41, 1, '2018-08-29', '12:00:00', '15:30:00', 0, NULL, NULL),
(42, 1, '2018-08-29', '08:00:00', '12:00:00', 0, NULL, NULL),
(43, 1, '2018-08-30', '10:00:00', '13:30:00', 0, NULL, NULL),
(44, 1, '2018-08-30', '08:00:00', '12:00:00', 0, NULL, NULL),
(45, 1, '2018-08-30', '12:00:00', '15:30:00', 0, NULL, NULL),
(46, 1, '2018-08-30', '12:00:00', '15:30:00', 0, NULL, NULL),
(47, 1, '2018-08-30', '15:30:00', '19:00:00', 0, NULL, NULL),
(48, 1, '2018-08-30', '15:30:00', '19:00:00', 0, NULL, NULL),
(49, 1, '2018-08-30', '17:00:00', '20:30:00', 0, NULL, NULL),
(50, 1, '2018-08-30', '17:00:00', '20:30:00', 0, NULL, NULL),
(51, 1, '2018-08-30', '19:00:00', '22:00:00', 0, NULL, NULL),
(52, 1, '2018-08-28', '10:00:00', '13:30:00', 0, NULL, NULL),
(53, 1, '2018-08-30', '08:00:00', '12:00:00', 0, NULL, NULL),
(54, 1, '2018-08-30', '19:00:00', '22:00:00', 0, NULL, NULL),
(55, 1, '2018-08-31', '17:00:00', '20:30:00', 0, NULL, NULL),
(56, 1, '2018-08-31', '19:00:00', '22:00:00', 0, NULL, NULL),
(57, 1, '2018-08-31', '08:00:00', '12:00:00', 0, NULL, NULL),
(58, 1, '2018-08-31', '17:00:00', '20:30:00', 0, NULL, NULL),
(59, 1, '2018-08-31', '15:30:00', '19:00:00', 0, NULL, NULL),
(60, 1, '2018-08-31', '15:30:00', '19:00:00', 0, NULL, NULL),
(61, 1, '2018-08-31', '12:00:00', '15:30:00', 0, NULL, NULL),
(62, 1, '2018-08-31', '12:00:00', '15:30:00', 0, NULL, NULL),
(63, 1, '2018-08-31', '08:00:00', '12:00:00', 0, NULL, NULL),
(64, 1, '2018-08-31', '10:00:00', '13:30:00', 0, NULL, NULL),
(65, 1, '2018-08-31', '19:00:00', '22:00:00', 0, NULL, NULL),
(66, 1, '2018-09-01', '10:00:00', '13:30:00', 0, NULL, NULL),
(67, 1, '2018-09-01', '08:00:00', '12:00:00', 0, NULL, NULL),
(68, 1, '2018-09-01', '12:00:00', '15:30:00', 0, NULL, NULL),
(69, 1, '2018-09-01', '12:00:00', '15:30:00', 0, NULL, NULL),
(70, 1, '2018-09-01', '15:30:00', '19:00:00', 0, NULL, NULL),
(71, 1, '2018-09-01', '15:30:00', '19:00:00', 0, NULL, NULL),
(72, 1, '2018-09-01', '17:00:00', '20:30:00', 0, NULL, NULL),
(73, 1, '2018-09-01', '08:00:00', '12:00:00', 0, NULL, NULL),
(74, 1, '2018-09-01', '19:00:00', '22:00:00', 0, NULL, NULL),
(75, 1, '2018-09-01', '17:00:00', '20:30:00', 0, NULL, NULL),
(76, 1, '2018-09-01', '19:00:00', '22:00:00', 0, NULL, NULL),
(77, 1, '2018-09-02', '17:00:00', '20:30:00', 0, NULL, NULL),
(78, 1, '2018-09-02', '08:00:00', '12:00:00', 0, NULL, NULL),
(79, 1, '2018-09-02', '19:00:00', '22:00:00', 0, NULL, NULL),
(80, 1, '2018-09-02', '17:00:00', '20:30:00', 0, NULL, NULL),
(81, 1, '2018-09-02', '15:30:00', '19:00:00', 0, NULL, NULL),
(82, 1, '2018-09-02', '15:30:00', '19:00:00', 0, NULL, NULL),
(83, 1, '2018-09-02', '12:00:00', '15:30:00', 0, NULL, NULL),
(84, 1, '2018-09-02', '12:00:00', '15:30:00', 0, NULL, NULL),
(85, 1, '2018-09-02', '08:00:00', '12:00:00', 0, NULL, NULL),
(86, 1, '2018-09-02', '10:00:00', '13:30:00', 0, NULL, NULL),
(87, 1, '2018-09-02', '19:00:00', '22:00:00', 0, NULL, NULL),
(88, 1, '2018-08-27', '09:00:00', '12:30:00', 1, NULL, NULL),
(89, 1, '2018-08-28', '09:00:00', '12:30:00', 1, NULL, NULL),
(90, 1, '2018-08-29', '09:00:00', '12:30:00', 1, NULL, NULL),
(92, 1, '2018-08-30', '09:00:00', '12:30:00', 1, NULL, NULL),
(93, 1, '2018-08-31', '09:00:00', '12:30:00', 1, NULL, NULL),
(94, 1, '2018-09-01', '09:00:00', '12:30:00', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apellido` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha_nac` date NOT NULL,
  `activo` tinyint(1) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre`, `apellido`, `email`, `password`, `fecha_nac`, `activo`, `remember_token`) VALUES
(1, 'Sin', 'Asignar', '', '', '1990-07-06', 0, ''),
(2, 'Mathias', 'Velilla', 'mathias.velilla@gmail.com', '$2a$04$HcGoj9s1/0kFWLiijuCX9OuUBXpPSQ6a/MsVO0kQc.dSFepj5Li/.', '1993-06-24', 1, 'P14ygoQyUWiRZlGhW8A2Y86UMnLmvLgnMRMHyqUF66tVXSc3woYLxTlpDZon'),
(3, 'Zully', 'Chang', 'zully.chang@gmail.com', '$2a$04$HcGoj9s1/0kFWLiijuCX9OuUBXpPSQ6a/MsVO0kQc.dSFepj5Li/.', '1990-11-19', 1, 'mykQq1nBQkqsdGIIrJI3Xq5j1H6NPtQXZunZbTZODKeJFadHPegyufMOocJz'),
(4, 'Usuario', '1', 'usuario1@gmail.com', '$2a$04$HcGoj9s1/0kFWLiijuCX9OuUBXpPSQ6a/MsVO0kQc.dSFepj5Li/.', '1990-11-19', 1, ''),
(5, 'Usuario', '2', 'usuario2@gmail.com', '$2a$04$HcGoj9s1/0kFWLiijuCX9OuUBXpPSQ6a/MsVO0kQc.dSFepj5Li/.', '1990-11-19', 1, '');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `asignaciones_turnos`
--
ALTER TABLE `asignaciones_turnos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `dias`
--
ALTER TABLE `dias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `perfiles`
--
ALTER TABLE `perfiles`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `perfiles_usuarios`
--
ALTER TABLE `perfiles_usuarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `planillas`
--
ALTER TABLE `planillas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `queqe`
--
ALTER TABLE `queqe`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `turnos`
--
ALTER TABLE `turnos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `asignaciones_turnos`
--
ALTER TABLE `asignaciones_turnos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;

--
-- AUTO_INCREMENT de la tabla `dias`
--
ALTER TABLE `dias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `perfiles`
--
ALTER TABLE `perfiles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `perfiles_usuarios`
--
ALTER TABLE `perfiles_usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `planillas`
--
ALTER TABLE `planillas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `queqe`
--
ALTER TABLE `queqe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `turnos`
--
ALTER TABLE `turnos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
