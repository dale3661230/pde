@extends('layout')

@section('content')

<!-- CONTENIDO -->

<style>

	tr {
	    border-bottom: 1px solid black;
	    border-top: 1px solid black;
	    border-collapse: collapse;
	}
	#bloqueHr {
		list-style-type:none;
		margin-bottom: 0px;
	}

	#bloqueHr > li {
		display: inline-block;
	}

</style>


<?php

$comienza = new DateTime($planilla->fecha_comienzo);
$termina = new DateTime($planilla->fecha_termino);

$interval = DateInterval::createFromDateString('1 day');
$period = new DatePeriod($comienza, $interval, $termina);

?>

<!-- Content Header (Page header) -->
<section class="content-header" index="2">
    <h1>
        Planilla 
        <small>Optional description</small>
    </h1>
    <div class="breadcrumb">
      {{--   <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li> --}}
        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modalEnviarPlanilla">Tomar Turnos</button>
    	<button type="button" class="btn btn-success">Crear Planilla</button>
    </div>
</section>
<!-- Main content -->
<section class="content container-fluid">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Hover Data Table</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<table id="example2" class="table table-bordered table-hover">
						<tr>
							<th>HORARIO/FECHA</th>
							<?php foreach ($period as $dt) { ?>
								<th><?= $dt->format("l Y-m-d"); ?></th>
							<?php } ?>
						</tr>
						<?php foreach ($planilla->horarios as $key => $bloqueHr) { ?>
							<tr>
								<!-- HORARIO FECHA --> 
								<td><?= $key ?></td> 
								<!-- DIAS -->
								<?php foreach ($bloqueHr as $key => $day) { ?>
									<td>
										<ul id="bloqueHr">
											<?php foreach ($day as $key => $turno) { ?>
												<li><?= $turno->nombre.' '.$turno->apellido?></li>
											<?php } ?>
										</ul>
									</td> 
								<?php } ?>		
							</tr>
						<?php } ?>
					</table>
				</div>
			<!-- /.box-body -->
			</div>
		</div>
	</div>
</section>


<?php
//Se RESETAN LAS VARIABLES
$comienza = new DateTime($planilla->fecha_comienzo);
?>
<!-- Modal -->
<div id="modalEnviarPlanilla" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Modal Header</h4>
			<p>Click on this paragraph</p>
		</div>
      	<div class="modal-body">

			<table id="planilla-modal" class="table table-bordered">
				<tr>
					<th>HORARIO/FECHA</th>
					<?php foreach ($period as $dt) { ?>
						<th><?= $dt->format("l Y-m-d"); ?></th>
					<?php } ?>
				</tr>
				<tr>
					 <!-- HORARIO --> 
					<td align="center" style="padding: 0px;">8:00 - 12:00</td>
					<!-- DIAS -->
					<?php foreach ($period as $dt) { ?>

						<!-- <td class="turno" style="padding: 0px;" hr-inicio="8:00" hr-termino="12:00" dia="<?= $dt->format("Y-m-d"); ?>" >
							<button type="button" class="btn btn-primary btn-sm" style="height: 100%; width: 100%;">CLICK</button>
						</td> -->

						<td class="turno" style="padding: 0px;" hr-inicio="8:00" hr-termino="12:00" dia="<?= $dt->format("Y-m-d"); ?>" >
						<div class="btn-group" data-toggle="buttons" style="width: 100%">
							<label class="btn btn-primary" style="width: 100%">
					            <input type="checkbox" name="fruit" id="apple" value="apple">apple
					        </label>
						</div>
						</td>
					<?php } ?>

				</tr>
				<tr>
					 <!-- HORARIO --> 
					<td align="center" style="padding: 0px;">10:00 - 13:30</td>
					<!-- DIAS -->
					<?php foreach ($period as $dt) { ?>
						<td class="turno" style="padding: 0px;" hr-inicio="10:00" hr-termino="13:30" dia="<?= $dt->format("Y-m-d"); ?>">
							<button type="button" class="btn btn-primary btn-sm" style="height: 100%; width: 100%;">CLICK</button>
						</td>
					<?php } ?>

				</tr>
				<tr>
					 <!-- HORARIO --> 
					<td align="center" style="padding: 0px;">11:00 - 15:00</td>
					<!-- DIAS -->
					<?php foreach ($period as $dt) { ?>
						<td class="turno" style="padding: 0px;" hr-inicio="11:00" hr-termino="15:00" dia="<?= $dt->format("Y-m-d"); ?>">
							<button type="button" class="btn btn-primary btn-sm" style="height: 100%; width: 100%;">CLICK</button>
						</td>
					<?php } ?>

				</tr>
				<tr>
					 <!-- HORARIO --> 
					<td align="center" style="padding: 0px;">12:00 - 15:30</td>
					<!-- DIAS -->
					<?php foreach ($period as $dt) { ?>
						<td class="turno" style="padding: 0px;" hr-inicio="12:00" hr-termino="15:30" dia="<?= $dt->format("Y-m-d"); ?>">
							<button type="button" class="btn btn-primary btn-sm" style="height: 100%; width: 100%;">CLICK</button>
						</td>
					<?php } ?>

				</tr>
				<tr>
					 <!-- HORARIO --> 
					<td align="center" style="padding: 0px;">15:30 - 19:00</td>
					<!-- DIAS -->
					<?php foreach ($period as $dt) { ?>
						<td class="turno" style="padding: 0px;" hr-inicio="15:30" hr-termino="19:00" dia="<?= $dt->format("Y-m-d"); ?>">
							<button type="button" class="btn btn-primary btn-sm" style="height: 100%; width: 100%;">CLICK</button>
						</td>
					<?php } ?>

				</tr>
				<tr>
					 <!-- HORARIO --> 
					<td align="center" style="padding: 0px;">17:00 - 20:30</td>
					<!-- DIAS -->
					<?php foreach ($period as $dt) { ?>
						<td class="turno" style="padding: 0px;" hr-inicio="17:00" hr-termino="20:30" dia="<?= $dt->format("Y-m-d"); ?>">
							<button type="button" class="btn btn-primary btn-sm" style="height: 100%; width: 100%;">CLICK</button>
						</td>
					<?php } ?>

				</tr>
				<tr>
					 <!-- HORARIO --> 
					<td align="center" style="padding: 0px;">19:00 - 22:00</td>
					<!-- DIAS -->
					<?php foreach ($period as $dt) { ?>
						<td class="turno" style="padding: 0px;" hr-inicio="19:00" hr-termino="22:00" dia="<?= $dt->format("Y-m-d"); ?>">
							<button type="button" class="btn btn-primary btn-sm" style="height: 100%; width: 100%;">CLICK</button>
						</td>
					<?php } ?>

				</tr>
				<tr>
					 <!-- HORARIO --> 
					<td align="center" style="padding: 0px;">22:00 - 00:00</td>
					<!-- DIAS -->
					<?php $i = 1; ?>
					<?php foreach ($period as $dt) { ?>
						<?php if($i<=5){ ?>
							<td class="turno" style="padding: 0px;" >
								<button type="button" class="btn btn-primary btn-sm disabled" style="height: 100%; width: 100%;">---</button>
							</td>
						<?php } else { ?>
							<td class="turno" style="padding: 0px;" hr-inicio="22:00" hr-termino="00:00" dia="<?= $dt->format("Y-m-d"); ?>">
								<button type="button" class="btn btn-primary btn-sm" style="height: 100%; width: 100%;">CLICK</button>
							</td>
						<?php } ?>
						<?php $i++; ?>
					<?php } ?>

				</tr>
			</table>

      	</div>
      	<div class="modal-footer">
        	<button type="button" class="btn btn-danger" id="closeModal" data-dismiss="modal">Cerrar</button>
        	<button type="button" class="btn btn-success" id="botonEnviarTurnos">Enviar Turnos</button>
      	</div>
    </div>

  </div>
</div>

<script src="{{ asset('adminlte/bower_components/jquery/dist/jquery.min.js') }}"></script>

<script>
	$( document ).ready(function() {

		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
			}
		});

		// Para que cuando el modal se cierre sus styles este se reinicie
		$("#modalEnviarPlanilla").on("hidden.bs.modal", function () {
		  	var queqe = []
		})

		var queqe = [] // Cola para guardar
		// Script para cambiar color al hacer click	y seleccionar turnos
		$(".turno").click(function(){
	            //alert("Turno tomado");
	        //$(this).css("background-color","black");
	        var hrInicio = $(this).attr("hr-inicio");
	        var hrTermino = $(this).attr("hr-termino");
	        var dia = $(this).attr("dia");

	        var turno = [dia, hrInicio, hrTermino];
  			queqe.push(turno);

	    });

	    $("#botonEnviarTurnos").click(function(){
	    	console.log(queqe);
	    	$.ajax({
	    		url: "{{ route('enviarTurnosPlanilla') }}",
	    		type: 'POST',
	    		data:{
	    			queqe: queqe 
	    		}
	    	})
	    	.done(function() {
	    		queqe = []; // Limpiar Cola
				alert( "success" );
			})
			.fail(function() {
			    alert( "error" );
			});
	    });
    });



</script>

@stop

