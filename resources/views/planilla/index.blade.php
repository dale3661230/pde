@extends('layout')

@section('content')

<!-- CONTENIDO -->

<?php

$comienza = new DateTime($planilla->fecha_comienzo);
$termina = new DateTime($planilla->fecha_termino);

$interval = DateInterval::createFromDateString('1 day');
$period = new DatePeriod($comienza, $interval, $termina);

?>

<style>

	tr {
	    border-bottom: 1px solid black;
	    border-top: 1px solid black;
	    border-collapse: collapse;
	}
	#bloqueHr {
		list-style-type:none;
		margin-bottom: 0px;
	}

	#bloqueHr > li {
		display: inline-block;
	}

</style>

<!-- Content Header (Page header) -->
<section class="content-header" index="2">
    <h1>
        Planilla 
        <small>Optional description</small>
    </h1>
    <div class="breadcrumb">
      {{--   <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li> --}}
        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modalEnviarPlanilla">Tomar Turnos</button>
      	<button type="button" class="btn btn-success">Crear Planilla</button>
    </div>
</section>

<!-- Main content -->
<section class="content container-fluid">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Hover Data Table</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                	<?php //dd($turnos); ?>
                	<div class="table-responsive">
	                    <table id="example2" class="table table-bordered table-hover">
							<tr>
								<th>HORARIO/FECHA</th>
								<?php foreach ($period as $dt) { ?>
									<th class="text-center"><?= $dt->format("l Y-m-d"); ?></th>
								<?php } ?>
							</tr>
							<?php foreach ($planilla->horarios as $key => $bloqueHr) { ?>
								<?php //dd($bloqueHr); ?>
								<tr>
									<!-- HORARIO FECHA --> 
									<td class="text-center" ><?= date("H:i", strtotime($bloqueHr['hora_inicio'])) ?> - <?= date("H:i", strtotime($bloqueHr['hora_termino']))?></td> 
									<!-- DIAS -->
									<?php foreach ($bloqueHr['dias'] as $key => $day) { ?>
										<td>
											<ul id="bloqueHr">
												<?php foreach ($day as $key => $turno) { ?>
													<li><?= $turno->nombre.' '.$turno->apellido?></li>
												<?php } ?>
											</ul>
										</td> 
									<?php } ?>		
								</tr>
							<?php } ?>
						</table>
					</div>

                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
</section>


<!-- MODAL -->
<div id="modalEnviarPlanilla" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Modal Header</h4>
			<p>Click on this paragraph</p>
		</div>
      	<div class="modal-body">

      		<table id="example2" class="table table-bordered table-hover">
				<tr>
					<th>HORARIO/FECHA</th>
					<?php foreach ($period as $dt) { ?>
						<th><?= $dt->format("l"); ?></th>
					<?php } ?>
				</tr>
				<?php foreach ($planilla->horarios as $key => $bloqueHr) { ?>
					<tr>
						<!-- HORARIO FECHA --> 
						<td class="text-center"><?= date("H:i", strtotime($bloqueHr['hora_inicio'])) ?> - <?= date("H:i", strtotime($bloqueHr['hora_termino']))?></td> 
						<!-- DIAS -->
						<?php foreach ($bloqueHr['dias'] as $key => $day) { ?>
							<?php //dd($key); ?>
							<td class="" style="padding: 0px;">
								<div class="btn-group" data-toggle="buttons" style="width: 100%">
									<label class="btn btn-primary boton-turno" style="width: 100%" hr-inicio="<?= $bloqueHr['hora_inicio'] ?>" hr-termino="<?= $bloqueHr['hora_termino']?>" dia="<?= $key ?>">
							            <input type="checkbox" name="fruit" id="apple" value="apple">CLICK
							        </label>
								</div>
							</td> 
						<?php } ?>		
					</tr>
				<?php } ?>
			</table>

      	</div>
      	<div class="modal-footer">
        	<button type="button" class="btn btn-danger" id="closeModal" data-dismiss="modal">Cerrar</button>
      	</div>
    </div>

  </div>
</div>

<script>
    $(function () {

    	$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
			}
		});

		// Script para cambiar color al hacer click	y seleccionar turnos
		$(".boton-turno").click(function(){

			if ($(this).hasClass('active')) {
				alert("Sacar de cola");
	  		}else{
	  			// alert("Agregar a cola");
	  			var hrInicio = $(this).attr("hr-inicio");
		        var hrTermino = $(this).attr("hr-termino");
		        var dia = $(this).attr("dia");
		        // console.log(hrInicio)
		        // console.log(hrTermino)
		        // console.log(dia);
		        $.ajax({
		    		url: "{{ route('enviarTurnosPlanilla') }}",
		    		type: 'POST',
		    		data:{
		    			dia: dia,
		    			hora_inicio: hrInicio,
		    			hora_termino: hrTermino
		    		}
	    		})
		    	.done(function() {
					alert( "success" );
				})
				.fail(function() {
				    alert( "error" );
				});

		  	}

	    });

	    /* initialize the calendar
	     -----------------------------------------------------------------*/
	    //Date for the calendar events (dummy data)
	    var date = new Date()
	    var d    = date.getDate(),
	        m    = date.getMonth(),
	        y    = date.getFullYear()
	    $('#calendar').fullCalendar({

	        defaultView: 'agendaWeek',
	        minTime: '08:00:00',
	        maxTime: '24:00:00',
	        allDaySlot: false,
	        header    : {
	            left  : 'prev,next',
	            center: 'title',
	            right : ''
	        },
	        buttonText: {
	            today: 'today',
	            week : 'week',
	        },

	        //Random default events
	        events    : [
	        <?php foreach ($turnos as $key => $turno) { ?>
		        {
		            title          : '<?=$turno->nombre ?>',
		            start          : '<?=$turno->fecha?>T<?=$turno->hora_inicio?>',
		            end            : '<?=$turno->fecha?>T<?=$turno->hora_termino?>',
		            backgroundColor: '#f56954', //red
		            borderColor    : '#f56954', //red
		        },
		    <?php  } ?>
	        ],
	        editable  : false,
	        droppable : true, // this allows things to be dropped onto the calendar !!!
	        drop      : function (date, allDay) { // this function is called when something is dropped

	        // retrieve the dropped element's stored Event Object
	        var originalEventObject = $(this).data('eventObject')

	        // we need to copy it, so that multiple events don't have a reference to the same object
	        var copiedEventObject = $.extend({}, originalEventObject)

	        // assign it the date that was reported
	        copiedEventObject.start           = date
	        copiedEventObject.allDay          = allDay
	        copiedEventObject.backgroundColor = $(this).css('background-color')
	        copiedEventObject.borderColor     = $(this).css('border-color')

	        // render the event on the calendar
	        // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
	        $('#calendar').fullCalendar('renderEvent', copiedEventObject, true)

	        // is the "remove after drop" checkbox checked?
	        if ($('#drop-remove').is(':checked')) {
	          // if so, remove the element from the "Draggable Events" list
	          $(this).remove()
	        }

	      }
	    })
    });
</script>

@stop

