@extends('layout')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header" index="4">
    <h1>
        ADMINISTRACION DE USUARIOS
        <small>Optional description</small>
    </h1>
    <ol class="breadcrumb">
        <!-- <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li> -->
      	<button type="button" class="btn btn-success" id="crear-usuario" data-toggle="modal" data-target="#modalCrearUsuario">Crear Usuario</button>
    </ol>
</section>
<!-- Main content -->
<section class="content container-fluid">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title"></h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<table class="table table-bordered table-striped" id="tabla-usuarios">
						<thead>
							<tr>
								<th class="text-center">NOMBRE</th>
								<th class="text-center">APELLIDO</th>
								<th class="text-center">EMAIL</th>
								<th class="text-center">ESTADO</th>
								<th class="text-center">ACCIONES</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($usuarios as $key => $usuario) { ?>
								<tr>
									<td class="text-center"><?= $usuario->nombre ?></td>
									<td class="text-center"><?= $usuario->apellido ?></td>
									<td class="text-center"><?= $usuario->email ?></td>
									<td class="text-center"></td>
									<td class="text-center">
										<div class="btn-group">
					                        <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown">
					                        	<span class="caret"></span>
					                        </button>
					                        <ul class="dropdown-menu">
					                        	<li><a href="#">Editar</a></li>
					                          	<li><a href="#">Eliminar</a></li>
					                        </ul>
					                     </div>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			<!-- /.box-body -->
			</div>
		</div>
	</div>
</section>

<!-- MODAL CREAR USUARIO -->
<div class="modal fade" id="modalCrearUsuario">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Default Modal</h4>
			</div>
			<div class="modal-body">
				<p>One fine body&hellip;</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
				<button type="button" class="btn btn-success pull-left">Crear Usuario</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
<!-- /.modal-dialog -->
</div>


<!-- DataTables -->
<script src="{{ asset('adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>

<script>
$(function () {
    $('#tabla-usuarios').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>

@stop