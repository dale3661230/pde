<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title></title>
	<link rel="stylesheet" href="../public/css/app.css">
</head>
<body>
	<div class="container">
		@if (session()->has('flash'))
			<div class="alert alert-info">{{ session('flash') }}</div>
		@endif
		@yield('content')
	</div>
</body>
</html>