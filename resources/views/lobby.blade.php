@extends('layout')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header" index="1">
    <h1>
        LOBBY
        <small>Optional description</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
    </ol>
</section>
<!-- Main content -->
<section class="content container-fluid">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title"></h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					LOBBY
				</div>
			<!-- /.box-body -->
			</div>
		</div>
	</div>
</section>

@stop