<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTurnosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('turnos', function (Blueprint $table) {
            
            $table->increments('id');
            $table->integer('id_planilla');
            $table->date('fecha');
            $table->time('hora_inicio');
            $table->time('hora_termino');
            $table->integer('id_usuario');
            $table->boolean('disponibilidad');
            $table->boolean('activo');
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('turnos');
    }
}
