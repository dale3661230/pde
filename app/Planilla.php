<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Planilla extends Model
{   
    
    protected $table = 'planillas';


    public function turnos()
    {
        return $this->hasMany('App\Turno', 'id_planilla');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'fecha_comienzo', 'fecha_termino'];

}
