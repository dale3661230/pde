<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Queqe extends Model
{   

    protected $table = 'queqe';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'id_planilla', 'id_usuario', 'fecha_turno','hora_inicio','hora_termino','created_at','updated_at','activo'];

}
