<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class AsignacionTurno extends Model
{   
    
    use Notifiable;

    protected $table = 'asignaciones_turnos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','id_planilla', 'id_turno', 'id_usuario', 'disponibilidad','activo'];

}
