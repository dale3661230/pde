<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;


class Usuario extends Authenticatable
{   
    
    use Notifiable;

    protected $table = 'usuarios';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nombre', 'email', 'password'];

    protected $hidden = [
        'password', 'remember_token'
    ];

    public function perfiles()
    {
        return $this->hasMany('App\PerfilUsuario','id_usuario');
    }

}
