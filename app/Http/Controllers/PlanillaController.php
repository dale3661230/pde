<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Planilla;
use App\Turno;
use App\Usuario;
use App\Queqe;


class PlanillaController extends Controller
{


	public function index()
	{

		//TRAE LA COLA
		$perfiles = DB::table('perfiles');

		$asignacion_pefiles = DB::table('perfiles_usuarios')
	        ->joinSub($perfiles, 'perfiles', function($join) {
	            $join->on('perfiles_usuarios.id_perfil', '=', 'perfiles.id');
	        });

	    $usuarios_perfiles = DB::table('usuarios')
	        ->joinSub($asignacion_pefiles, 'asignacion', function($join) {
	            $join->on('usuarios.id', '=', 'asignacion.id_usuario');
	        })->get();


	   	$perfiles = DB::table('perfiles_usuarios')
	   	  ->join('perfiles', 'perfiles.id', '=', 'perfiles_usuarios.id_usuario')

	   	$usuarios = DB::table('usuarios')
	   		 ->join($perfiles, 'perfiles.id', '=', 'perfiles_usuarios.id_usuario')

	    dd($usuarios_perfiles);



		// $Turnos = DB::table('queqe')
		// 	->join('usuarios', 'usuarios.id', '=', 'queqe.id_usuario')
		// 	->join('perfiles_usuarios', 'perfiles_usuarios.id_usuario', '=', 'usuarios.id')
		// 	->get();

		// dd($Turnos);

		$planilla =  DB::table('planillas')->first();

		//TURNOS DE LOS ASIGNADOS
		$turnosAsignados = DB::table('asignaciones_turnos')
			->join('turnos', 'asignaciones_turnos.id_turno', '=', 'turnos.id')
			->join('usuarios', 'asignaciones_turnos.id_usuario', '=', 'usuarios.id')
			->where([
				['asignaciones_turnos.id_planilla','=',$planilla->id],
			])
			->orderBy('turnos.hora_inicio','ASC')
			->orderBy('turnos.fecha','ASC')
			->get();

		$data = $planilla;
		$data->horarios = array();
		//dd($data);
		$count = 1;

		foreach ($turnosAsignados as $key => $turno) {
			//dd($turno);
			if(!array_key_exists($turno->hora_inicio, $data->horarios))
			{
				$data->horarios[$turno->hora_inicio] = array();
				$data->horarios[$turno->hora_inicio]['hora_inicio']=$turno->hora_inicio;
				$data->horarios[$turno->hora_inicio]['hora_termino']=$turno->hora_termino;
				$data->horarios[$turno->hora_inicio]['dias'][$turno->fecha] = array();
				array_push($data->horarios[$turno->hora_inicio]['dias'][$turno->fecha],$turno);
				

			}else {

				if(!array_key_exists($turno->fecha, $data->horarios[$turno->hora_inicio]['dias'])){
					$data->horarios[$turno->hora_inicio]['dias'][$turno->fecha] = array();
					array_push($data->horarios[$turno->hora_inicio]['dias'][$turno->fecha],$turno);
				}else {
					array_push($data->horarios[$turno->hora_inicio]['dias'][$turno->fecha],$turno);
				}

			}

		}

		//dd($data);

		//PLANILLA JS PARA EL USUARIO
		$turnosPropios = DB::table('asignaciones_turnos')
			->join('turnos', 'asignaciones_turnos.id_turno', '=', 'turnos.id')
			->join('usuarios', 'asignaciones_turnos.id_usuario', '=', 'usuarios.id')
			->where([
				['asignaciones_turnos.id_planilla','=',$planilla->id],
				['asignaciones_turnos.id_usuario','=',auth()->user()->id]
			])
			->get();

		return view('planilla.index',[
                'planilla' => $data,
                'turnos' => $turnosPropios
            ]);


	}

	public function enviarTurnosPlanilla(Request $request)
    {
    	$return = [];
    	$data = $request->all(); // This will get all the request data.
    	//dd($data);

    	if (!empty($data)) {

    		$queqe = new Queqe;
    		$queqe->id_planilla = 1; //Detectar id
    		$queqe->id_usuario = auth()->user()->id;
    		$queqe->fecha_turno = $data['dia'];
    		$queqe->hora_inicio = $data['hora_inicio'];
    		$queqe->hora_termino = $data['hora_termino'];
    		$queqe->activo = 1;

	 		if($queqe->save()){
	 			$return ['status'] = true;
    			$return ['msg'] = 'Exito';
	 		}else{
	 			$return ['status'] = false;
    			$return ['msg'] = 'Error';
	 		}
    	}else{
    		$return ['status'] = false;
    		$return ['msg'] = 'Datos vacios.';
    	}






    	//AGREGAR ASIGNACION DE TIPO DISPONIBILIDAD

			// DB::table('queqe')->insert([
			// 	'id_planilla' => 1, //PLANILLA A GENERAR
			// 	'id_turno' => $turno->id,
			// 	'id_usuario' => auth()->user()->id,
			// 	'disponibilidad' => 1,
			// 	'activo' => 1,
			// 	"created_at" =>  \Carbon\Carbon::now(), # \Datetime()
	 	//      	"updated_at" => \Carbon\Carbon::now(),  # \Datetime()
			// ]);

    //ALGORITMO PARA DETERMINAR LA PLANILLA
	// public function distribucionDeTurnos()
	// {
		

	// }


    }

}