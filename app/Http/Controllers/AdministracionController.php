<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Planilla;
use App\Turno;
use App\Usuario;

class AdministracionController extends Controller
{

	public function index()
	{
		return view('administracion.index');
	}

	public function usuarios()
	{

		$usuarios = DB::table('usuarios')
			->select('nombre', 'apellido', 'email')
			->where([
				['activo','=',1],
			])
			->get();

		//dd($usuarios);

		return view('administracion.usuarios',[
				'usuarios' => $usuarios
			]);
	}
}