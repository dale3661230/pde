<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Turno extends Model
{   
    
    use Notifiable;

    protected $table = 'turnos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','id_planilla', 'fecha', 'hora_inicio', 'hora_termino','activo'];

}
